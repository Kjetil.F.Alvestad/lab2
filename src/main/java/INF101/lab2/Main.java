package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated
        pokemon1 = new Pokemon("Snorlax");
        pokemon2 = new Pokemon("Mew");

        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println();

        IPokemon currentPokemon = pokemon1;
        IPokemon target = pokemon2;

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            currentPokemon.attack(target);
            IPokemon temp = currentPokemon;
            currentPokemon = target;
            target = temp;
        }


    }
}
